package com.clienteKafka.clienteKafka.model;

public class Cliente {

    public String getNome() {
        return nome;
    }

    public int getPorta() {
        return porta;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setPorta(int porta) {
        this.porta = porta;
    }

    private String nome;
    private  int porta;

}
