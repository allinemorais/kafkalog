package com.logKafka.logKafka;

import com.clienteKafka.clienteKafka.model.Cliente;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;


@Component
public class LogConsumer {

    @KafkaListener(topics = "spec2-alline-lopes-1", groupId = "alline_171")
    public void receber(@Payload Cliente cliente) {
        System.out.println("Recebi um livro chamado " + cliente.getNome() );
    }

    public void GerarCsv(){

        String filename = "users.csv";

    }
}
